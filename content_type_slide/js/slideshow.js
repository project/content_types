
// Drupal.behaviors.mobileslideshow = {
//     attach: function(context) {

//       jQuery('body', context).once('mobileslideshow', function () {
//         jQuery('body').bind('responsivelayout', function (e, d) {  
         
//           if (d.to == 'mobile') {
//             console.log('mobile');
//             // Hide the slideshow on mobile.
//             jQuery('#block-views-slideshow-slideshow-block').hide();
//             // Destroy the slideshow cycle so it doesnt run when hidden.
//             jQuery('#views_slideshow_cycle_teaser_section_slideshow-slideshow').cycle('destroy');
       
        
//           }
//           if (d.to == 'normal') {
//              console.log('normal');
//             // Only do this on pages with a slideshow.
//             if (jQuery('#block-views-slideshow-slideshow-block').length) {
              
//               // Show the slideshow.
//               jQuery('#block-views-slideshow-slideshow-block').show();
              
//               // Destroy the slishows cycle functionality.
//               jQuery('#views_slideshow_cycle_main_slideshow-slideshow_block').cycle('destroy');
             
//               // Unbind the click events on the pager fields.
//               jQuery('#block-views-slideshow-slideshow-block .views_slideshow_pager_field').children().each(function(index, pagerItem) {
//                 jQuery(pagerItem).unbind('click');                 
//               });
               
//               // Remove the processed classes from pager fields.
//               jQuery('#block-views-slideshow-slideshow-block .views-slideshow-pager-field-processed').removeClass('views-slideshow-pager-field-processed');
              
              
//               // Remove the processed class from the slideshow.
//               jQuery('#block-views-slideshow-slideshow-block .viewsSlideshowCycle-processed').removeClass('viewsSlideshowCycle-processed');
              
//               // Remove the processed classes from the controls.
//               jQuery('#block-views-slideshow-slideshow-block .views-slideshow-controls-text-previous-processed').removeClass('.views-slideshow-controls-text-previous-processed').unbind('click');
//               jQuery('#block-views-slideshow-slideshow-block .views-slideshow-controls-text-next-processed').removeClass('.views-slideshow-controls-text-next-processed').unbind('click');
              
//               // Remove the active class fro mthe current active pager item.
//               jQuery('#block-views-slideshow-slideshow-block .views_slideshow_pager_field_item').removeClass('active');
      
//               // Hide all the slides.
//               jQuery('#block-views-slideshow-slideshow-block .views_slideshow_cycle_slide').hide();
              
//               // Show the first slide.
//               //jQuery('#views_slideshow_cycle_div_slideshow-slideshow_0').show();
              
//               // Set the active class to be the first slides pager.
//            //   jQuery('#block-views-slideshow-slideshow-block #views_slideshow_pager_field_item_top_slideshow-slideshow_0').addClass('active');
       
//               var settings = Drupal.settings.viewsSlideshowCycle["#views_slideshow_cycle_main_slideshow-slideshow_block"];
//               settings.opts.startingSlide = undefined;
//               settings.processedAfter = undefined;
//               settings.processedBefore = undefined;
//               settings.loadedImages = 0;
//               settings.wait_for_image_load = 0;

//               // Reattach all pagers.
//               Drupal.behaviors.viewsSlideshowCycle.attach();
//               Drupal.behaviors.viewsSlideshowPagerFields.attach();
//               Drupal.behaviors.viewsSlideshowControlsText.attach();

//               // Goto the next slide.
//               Drupal.viewsSlideshowCycle.goToSlide({
// 				slideshowID: 'slideshow-slideshow-block',
// 				slideNum: 0
//               });
//            }
   
//           }
          
//         });
//       });
//     }
//   };