<?php
/**
 * @file
 * content_type_post.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function content_type_post_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'blog_categories';
  $context->description = 'Blog Categories';
  $context->tag = 'blog';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'blog*' => 'blog*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-blog_categories-blog_categories' => array(
          'module' => 'views',
          'delta' => 'blog_categories-blog_categories',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blog Categories');
  t('blog');
  $export['blog_categories'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'blog_related';
  $context->description = 'Related Posts';
  $context->tag = 'blog';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'post' => 'post',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-blog-related_posts' => array(
          'module' => 'views',
          'delta' => 'blog-related_posts',
          'region' => 'content',
          'weight' => '5',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Related Posts');
  t('blog');
  $export['blog_related'] = $context;

  return $export;
}
