<?php
/**
 * @file
 * content_type_vacancy.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function content_type_vacancy_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_vacancy_content|node|vacancy|form';
  $field_group->group_name = 'group_vacancy_content';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'vacancy';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Job Description',
    'weight' => '1',
    'children' => array(
      0 => 'body',
      1 => 'field_vacancy_benefits',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Job Description',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_vacancy_content|node|vacancy|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_vacancy_details|node|vacancy|form';
  $field_group->group_name = 'group_vacancy_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'vacancy';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Vacancy Details',
    'weight' => '2',
    'children' => array(
      0 => 'field_vacancy_category',
      1 => 'field_vacancy_quantity',
      2 => 'field_vacancy_type',
      3 => 'field_vacancy_reference',
      4 => 'field_vacancy_salary_extra',
      5 => 'field_vacancy_salary',
      6 => 'field_vacancy_skills',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Vacancy Details',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_vacancy_details|node|vacancy|form'] = $field_group;

  return $export;
}
