<article<?php print $attributes; ?>>
<a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a>
  <div<?php print $content_attributes; ?>>
    <?php
      print render($content['body']);
    ?>
  </div>

</article>