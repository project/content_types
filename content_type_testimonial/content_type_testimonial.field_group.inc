<?php
/**
 * @file
 * content_type_testimonial.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function content_type_testimonial_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_page_testimonial|node|page|form';
  $field_group->group_name = 'group_page_testimonial';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Testimonial',
    'weight' => '7',
    'children' => array(
      0 => 'field_page_testimonial_reference',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Testimonial',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => 'Use these fields to add one or more testimonials to the page.',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_page_testimonial|node|page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_testimonial_content|node|testimonial|form';
  $field_group->group_name = 'group_testimonial_content';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'testimonial';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Content',
    'weight' => '1',
    'children' => array(
      0 => 'body',
      1 => 'field_testimonial_author',
      2 => 'field_testimonial_image',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_testimonial_content|node|testimonial|form'] = $field_group;

  return $export;
}
