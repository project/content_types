<?php
/**
 * @file
 * content_type_client.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function content_type_client_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function content_type_client_node_info() {
  $items = array(
    'client' => array(
      'name' => t('Client'),
      'base' => 'node_content',
      'description' => t('Use <em>clients</em> to add your clients'),
      'has_title' => '1',
      'title_label' => t('Client Name'),
      'help' => '',
    ),
  );
  return $items;
}
