<article<?php print $attributes; ?>>
  <div<?php print $content_attributes; ?>>
    <?php
      print render($content['body']);
    ?>
  </div>
</article>