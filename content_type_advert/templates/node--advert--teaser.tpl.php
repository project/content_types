<?php
 


?>
<article<?php print $attributes; ?>>

<?php
  // link : $content['field_advert_url']['#items'][0]
  if (isset($content['field_advert_image'])) {
    if (isset($content['field_advert_url'])) {
    // with link
     print l(render($content['field_advert_image']),$content['field_advert_url']['#items'][0]['url'], array('html'=> TRUE)); 
    } else {
    // without link
     print render($content['field_advert_image']);
    }
   
  }
  print render($content['body']);
  
  if (isset($content['field_advert_url']) && ($content['field_advert_url']['#items'][0]['title'] != $content['field_advert_url']['#items'][0]['url']) ){
    print render($content['field_advert_url']);
  }
?>
  


</article>