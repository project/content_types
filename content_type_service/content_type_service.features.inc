<?php
/**
 * @file
 * content_type_service.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function content_type_service_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function content_type_service_node_info() {
  $items = array(
    'service' => array(
      'name' => t('Service'),
      'base' => 'node_content',
      'description' => t('Add a <em>service</em> that you offer. Services are listed together.'),
      'has_title' => '1',
      'title_label' => t('Service Name'),
      'help' => '',
    ),
  );
  return $items;
}
