<?php
/**
 * @file
 * content_type_advert.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function content_type_advert_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function content_type_advert_image_default_styles() {
  $styles = array();

  // Exported image style: advert_image.
  $styles['advert_image'] = array(
    'name' => 'advert_image',
    'effects' => array(),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function content_type_advert_node_info() {
  $items = array(
    'advert' => array(
      'name' => t('Advert'),
      'base' => 'node_content',
      'description' => t('Use <em>adverts</em> to promote things'),
      'has_title' => '1',
      'title_label' => t('Advert Title'),
      'help' => t('Use adverts to promote things'),
    ),
  );
  return $items;
}
