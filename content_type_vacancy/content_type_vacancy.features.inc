<?php
/**
 * @file
 * content_type_vacancy.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function content_type_vacancy_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function content_type_vacancy_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function content_type_vacancy_node_info() {
  $items = array(
    'cv' => array(
      'name' => t('CV'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'vacancy' => array(
      'name' => t('Vacancy'),
      'base' => 'node_content',
      'description' => t('Use <em>Vacancies</em> to advertise any available jobs.'),
      'has_title' => '1',
      'title_label' => t('Job Title'),
      'help' => '',
    ),
  );
  return $items;
}
