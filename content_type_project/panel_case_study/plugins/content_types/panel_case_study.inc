<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
    'title' => t('case_study Panel'),
    'description' => t('Add some text to a panel page'),
    // 'single' => TRUE means has no subtypes.
    'single' => TRUE,
    // Constructor.
    'content_types' => array('panel_case_study_content_type'),
    // Name of a function which will render the block.
    'render callback' => 'panel_case_study_content_type_render',
    // The default context.
    'defaults' => array(),
    'admin info' => 'panel_case_study_admin_info',
    // This explicitly declares the config form. Without this line, the func would be
    // ctools_plugin_example_no_context_content_type_edit_form.
    'edit form' => 'panel_case_study_content_type_edit_form',
    'category' => array(t('Custom Widgets'), -9),
        // this example does not provide 'admin info', which would populate the
        // panels builder page preview.
);

/**
 * Run-time rendering of the body of the block.
 *
 * @param $subtype
 * @param $conf
 *   Configuration as done at admin time.
 * @param $args
 * @param $context
 *   Context - in this case we don't have any.
 *
 * @return
 *   An object with at least title and content members.
 */
function panel_case_study_content_type_render($subtype, $conf, $args, $context) {
  $block = new stdClass();

  preg_match('/.*\[#(.*)\]/', $conf['item1'], $id_matches);
  $nid = $id_matches[1];
  
  $node = node_load($nid);

  $case_study = node_view($node);
 
  
  // get variables
  $pane_title = isset($conf['override_title_text']) ? $conf['override_title_text'] : '';
 


  // starter markup
  $markup = '';
  // only add stuff is there is something
  if ($pane_title ) {
    $markup = '
    <div class="pane-header ' . rtrim($pane_classes) . '">
      <h2 class="title">' . $pane_title . '</h2>'
      . $link_markup .
    '</div>';
  }
  // add title to content
  $block->content = $markup;
  $block->content .= '<div class="pane-content-inner">';
  $block->content .= drupal_render($case_study); //wysiwyg
  $block->content .= '</div>';


  return $block;
}

/**
 * 'Edit form' callback for the content type.
 * This example just returns a form; validation and submission are standard drupal
 * Note that if we had not provided an entry for this in hook_content_types,
 * this could have had the default name
 * ctools_plugin_example_no_context_content_type_edit_form.
 *
 */
function panel_case_study_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];


  // form options

  // Just construct a regular #type 'text_format' form element:
  $form['item1'] = array(
    '#type' => 'textfield',
    '#title' => 'Start typing the name of a case_study item to use in this panel',
   '#default_value' => isset($conf['item1']) ? $conf['item1'] : '',
     '#autocomplete_path' => 'case-studies/autocomplete',
  );
  
  return $form;
}

function panel_case_study_content_type_edit_form_submit(&$form, &$form_state) {
  $keys = array('item1');
  foreach ($keys as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}


/**
 * Ctools plugin function hook_admin_info.
 */
function panel_case_study_admin_info($subtype, $conf, $context = NULL) {

  $pane_title = isset($conf['override_title_text']) ? $conf['override_title_text'] : '';

  if ($function = ctools_plugin_get_function($plugin, 'admin info')) {
    $output = $function($subtype, $conf, $context);
  }
  if (empty($output) || !is_object($output)) {
    $output = new stdClass();
    $output->title = !empty($pane_title) ? $pane_title : t('No title set');
    $output->content = t('No info available.');
  }

  return $output;
}

