<?php
/**
 * @file
 * content_type_vacancy.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function content_type_vacancy_taxonomy_default_vocabularies() {
  return array(
    'vacancies' => array(
      'name' => 'Vacancies',
      'machine_name' => 'vacancies',
      'description' => 'Job Vacancies',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'vacancy_skills' => array(
      'name' => 'Vacancy Skills',
      'machine_name' => 'vacancy_skills',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'vacancy_types' => array(
      'name' => 'Vacancy Types',
      'machine_name' => 'vacancy_types',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
  );
}
