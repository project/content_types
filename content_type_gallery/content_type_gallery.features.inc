<?php
/**
 * @file
 * content_type_gallery.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function content_type_gallery_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function content_type_gallery_node_info() {
  $items = array(
    'image' => array(
      'name' => t('Image'),
      'base' => 'node_content',
      'description' => t('Use <em>images</em> to upload an image for use on the site. Images can be used to create galleries or be associated with a page.'),
      'has_title' => '1',
      'title_label' => t('Image'),
      'help' => '',
    ),
  );
  return $items;
}
