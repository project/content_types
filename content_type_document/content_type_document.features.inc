<?php
/**
 * @file
 * content_type_document.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function content_type_document_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function content_type_document_node_info() {
  $items = array(
    'document' => array(
      'name' => t('Document'),
      'base' => 'node_content',
      'description' => t('Use <em>documents</em> to add documents to the site'),
      'has_title' => '1',
      'title_label' => t('Document Name'),
      'help' => '',
    ),
  );
  return $items;
}
