<?php
/**
 * @file
 * content_type_page.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function content_type_page_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: main-menu.
  $menus['main-menu'] = array(
    'menu_name' => 'main-menu',
    'title' => 'Main menu',
    'description' => 'The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.',
  );
  // Exported menu: menu-footer-menu.
  $menus['menu-footer-menu'] = array(
    'menu_name' => 'menu-footer-menu',
    'title' => 'Footer Menu',
    'description' => 'A menu for use in the footer',
  );
  // Exported menu: menu-header-menu.
  $menus['menu-header-menu'] = array(
    'menu_name' => 'menu-header-menu',
    'title' => 'Header Menu',
    'description' => 'A menu for use in the header',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('A menu for use in the footer');
  t('A menu for use in the header');
  t('Footer Menu');
  t('Header Menu');
  t('Main menu');
  t('The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.');


  return $menus;
}
