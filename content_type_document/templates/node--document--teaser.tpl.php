<article<?php print $attributes; ?>>
  <div<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      
      
      
      print '<span class="file-title icon-' .pathinfo(file_create_url($node->field_document_file['und'][0]['uri']), PATHINFO_EXTENSION) . '">' . $title  . '</span>' .  l('Download',
          file_create_url($node->field_document_file['und'][0]['uri']),
          array(
            'html' => TRUE,
            'attributes' => array(
              'class' => array(
                pathinfo(file_create_url($node->field_document_file['und'][0]['uri']), PATHINFO_EXTENSION),
                'btn'
              )
              )
            )
          );
    ?>
  </div>
</article>
