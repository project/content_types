<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
    'title' => t('Post Panel'),
    'description' => t('Add some text to a panel page'),
    // 'single' => TRUE means has no subtypes.
    'single' => TRUE,
    // Constructor.
    'content_types' => array('panel_post_content_type'),
    // Name of a function which will render the block.
    'render callback' => 'panel_post_content_type_render',
    // The default context.
    'defaults' => array(),
    'admin info' => 'panel_post_admin_info',
    // This explicitly declares the config form. Without this line, the func would be
    // ctools_plugin_example_no_context_content_type_edit_form.
    'edit form' => 'panel_post_content_type_edit_form',
    'category' => array(t('Custom Widgets'), -9),
        // this example does not provide 'admin info', which would populate the
        // panels builder page preview.
);

/**
 * Run-time rendering of the body of the block.
 *
 * @param $subtype
 * @param $conf
 *   Configuration as done at admin time.
 * @param $args
 * @param $context
 *   Context - in this case we don't have any.
 *
 * @return
 *   An object with at least title and content members.
 */
function panel_post_content_type_render($subtype, $conf, $args, $context) {
  $block = new stdClass();

  
  // get variables
  $pane_title =  'Latest News';
 
  dsm($conf);
  
  // if both are set to all then get the latest news
  $query = new EntityFieldQuery();
  
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'article')
    ->propertyCondition('status', 1)
       ->addTag('random')
          ->range(0,1);
    
    
  if ($conf['sector'] == 'all' && $conf['campus'] == 'all') {
    

  } else if ($conf['sector'] == 'all') {
    
  } else if ($conf['campus'] == 'all'){
    
  } else {
    
  }
  
  $result = $query->execute();
   
   if (isset($result['node'])) {
    $nid = array_keys($result['node']);
  }
  
  
  $node = node_load($nid);
 
  $latest_news =   $node->title;
$latest_news .= '<br/> the rest of the fields which will come with the design';
  // starter markup
  $markup = '';
  // only add stuff is there is something
  if ($pane_title ) {
    $markup = '
    <div class="pane-header ' . rtrim($pane_classes) . '">
      <h2 class="title">' . $pane_title . '</h2>'
      . $link_markup .
    '</div>';
  }
  // add title to content
  $block->content = $markup;
  $block->content .= '<div class="pane-content-inner">';
  $block->content .= $latest_news; //wysiwyg
  $block->content .= '</div>';


  return $block;
}

/**
 * 'Edit form' callback for the content type.
 * This example just returns a form; validation and submission are standard drupal
 * Note that if we had not provided an entry for this in hook_content_types,
 * this could have had the default name
 * ctools_plugin_example_no_context_content_type_edit_form.
 *
 */
function panel_post_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];


   $form['sector'] = array(
    '#type' => 'select',
    '#title' => 'Choose a sector',
   '#options' => array('all' => 'All') 
  );
  
   
  $form['campus'] = array(
    '#type' => 'select',
    '#title' => 'Choose a campus',
   '#options' => array('all' => 'All') 
  );
  
  
  
 
  
  return $form;
}

function panel_post_content_type_edit_form_submit(&$form, &$form_state) {
  $keys = array('sector','campus');
  foreach ($keys as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}


/**
 * Ctools plugin function hook_admin_info.
 */
function panel_post_admin_info($subtype, $conf, $context = NULL) {

  $pane_title = isset($conf['override_title_text']) ? $conf['override_title_text'] : '';

  if ($function = ctools_plugin_get_function($plugin, 'admin info')) {
    $output = $function($subtype, $conf, $context);
  }
  if (empty($output) || !is_object($output)) {
    $output = new stdClass();
    $output->title = !empty($pane_title) ? $pane_title : t('No title set');
    $output->content = t('No info available.');
  }

  return $output;
}

