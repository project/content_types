<?php
/**
 * @file
 * content_type_gallery.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function content_type_gallery_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_image_details|node|image|form';
  $field_group->group_name = 'group_image_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'image';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Details',
    'weight' => '1',
    'children' => array(
      0 => 'field_image_image',
      1 => 'field_image_description',
      2 => 'field_page_gallery_category',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_image_details|node|image|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_page_gallery|node|page|form';
  $field_group->group_name = 'group_page_gallery';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Gallery',
    'weight' => '4',
    'children' => array(
      0 => 'field_page_gallery_reference',
      1 => 'field_page_gallery_tag',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Gallery',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => 'Use these fields to add a gallery to the page.',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_page_gallery|node|page|form'] = $field_group;

  return $export;
}
