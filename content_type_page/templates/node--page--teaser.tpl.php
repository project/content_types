<article<?php print $attributes; ?>>
  <?php print render($title_prefix); ?>
  <?php if (!$page && $title): ?>
   <a class="page-title" href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <div<?php print $content_attributes; ?>>
    <?php
      print render($content['body']);
    ?>
  </div>
</article>