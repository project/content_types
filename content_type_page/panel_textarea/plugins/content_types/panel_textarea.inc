<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
    'title' => t('Textarea Panel'),
    'description' => t('Add some text to a panel page'),
    // 'single' => TRUE means has no subtypes.
    'single' => TRUE,
    // Constructor.
    'content_types' => array('panel_textarea_content_type'),
    // Name of a function which will render the block.
    'render callback' => 'panel_textarea_content_type_render',
    // The default context.
    'defaults' => array(),
    'admin info' => 'panel_textarea_admin_info',
    // This explicitly declares the config form. Without this line, the func would be
    // ctools_plugin_example_no_context_content_type_edit_form.
    'edit form' => 'panel_textarea_content_type_edit_form',
    'category' => array(t('Custom Widgets'), -9),
        // this example does not provide 'admin info', which would populate the
        // panels builder page preview.
);

/**
 * Run-time rendering of the body of the block.
 *
 * @param $subtype
 * @param $conf
 *   Configuration as done at admin time.
 * @param $args
 * @param $context
 *   Context - in this case we don't have any.
 *
 * @return
 *   An object with at least title and content members.
 */
function panel_textarea_content_type_render($subtype, $conf, $args, $context) {
  $block = new stdClass();

  // get variables
  $pane_title = isset($conf['override_title_text']) ? $conf['override_title_text'] : '';
  $pane_link_text = isset($conf['f1']) ? $conf['f1'] : '';
  $pane_link_address = isset($conf['f2']) ? $conf['f2'] : '';
  $pane_link_rss = isset($conf['f3']) ? $conf['f3'] : '';
  $pane_gradient = isset($conf['f4']) ? $conf['f4'] : '';

  $link_markup = '';

  if ($pane_link_text) {
    $class = '';
    if ($pane_link_rss) {
      $class = 'divider';
    }
    $link_markup .= '<a href="' . $pane_link_address . '" class="page-icon ' . $class . '">' . $pane_link_text . '</a>';
  }
  if ($pane_link_rss) {
    $link_markup .= '<a title="Go to the RSS feed for: ' . $pane_title . '" class="rss-icon" href="' . $pane_link_rss . '">Go to the RSS feed for: ' . $pane_title . '</a>';
  }
  if (!empty($link_markup)) {
    $link_markup = '<div class="header-links">' . $link_markup . '</div>';
  }

  $pane_classes = '';
  if ($pane_gradient) {
    $pane_classes .= ' gradient ';
  }

  // starter markup
  $markup = '';
  // only add stuff is there is something
  if ($pane_title || $pane_link_text || $pane_link_address || $pane_link_rss || $pane_gradient) {
    $markup = '
    <div class="pane-header ' . rtrim($pane_classes) . '">
      <h2 class="title">' . $pane_title . '</h2>'
      . $link_markup .
    '</div>';
  }
  // add title to content
  $block->content = $markup;
  $block->content .= '<div class="pane-content-inner">';
  $block->content .= check_markup($conf['item1']['value'], 'full_html'); //wysiwyg
  $block->content .= '</div>';


  return $block;
}

/**
 * 'Edit form' callback for the content type.
 * This example just returns a form; validation and submission are standard drupal
 * Note that if we had not provided an entry for this in hook_content_types,
 * this could have had the default name
 * ctools_plugin_example_no_context_content_type_edit_form.
 *
 */
function panel_textarea_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
 
  // form options

  $defaults = array(
      'format' => filter_default_format(),
  );
  $my_richtext_field = variable_get('item1', $defaults);
  
  // Just construct a regular #type 'text_format' form element:
  $form['item1'] = array(
    '#type' => 'text_format',
    '#title' => 'Enter the text you would like to show in the panel',
    '#default_value' => !empty($conf['item1']['value']) ? $conf['item1']['value'] : '',
    '#format' => $my_richtext_field['format'],
    
  );
  // make sure the wysiwyg saves the content
  $form['buttons']['return']['#attributes'] = array('onclick' => 'tinyMCE.triggerSave(true,true);');
  
  // title settings
  $form['bcc_views_panes'] = array(
      '#type' => 'fieldset',
      '#title' => t('Title Options'),
  );
  $form['bcc_views_panes']['f1'] = array(
      '#type' => 'textfield',
      '#title' => t('Link Text'),
      '#default_value' => isset($conf['f1']) ? $conf['f1'] : '',
  );
  $form['bcc_views_panes']['f2'] = array(
      '#type' => 'textfield',
      '#title' => t('Link address'),
      '#default_value' => isset($conf['f2']) ? $conf['f2'] : '',
  );
  $form['bcc_views_panes']['f3'] = array(
      '#type' => 'textfield',
      '#title' => t('RSS Feed link address'),
      '#default_value' => isset($conf['f3']) ? $conf['f3'] : '',
  );
  $form['bcc_views_panes']['f4'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use gradient background'),
      '#default_value' => isset($conf['f4']) ? $conf['f4'] : '',
  );
  return $form;
}

function panel_textarea_content_type_edit_form_submit(&$form, &$form_state) {
  $keys = array('item1', 'f1', 'f2', 'f3', 'f4',);
  foreach ($keys as $key) {
    if ($key == 'item1'){
      //$form_state['item']->settings['body'] = $form_state['values'][$key]['value'];
      $form_state['conf']['item1']['value'] = $form_state['values']['item1']['value'];
    }
    else
    {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}


/**
 * Ctools plugin function hook_admin_info.
 */
function panel_textarea_admin_info($subtype, $conf, $context = NULL) {

  $pane_title = isset($conf['override_title_text']) ? $conf['override_title_text'] : '';

  if ($function = ctools_plugin_get_function($plugin, 'admin info')) {
    $output = $function($subtype, $conf, $context);
  }
  if (empty($output) || !is_object($output)) {
    $output = new stdClass();
    $output->title = !empty($pane_title) ? $pane_title : t('No title set');
    $output->content = t('No info available.');
  }

  return $output;
}